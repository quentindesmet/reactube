import Navbar from "react-bootstrap/Navbar";
import {Nav} from "react-bootstrap";
import SearchBar from './SearchBar';
import {useHistory} from "react-router-dom";

const MyNav = ({onResults, selectedType, selectType}) => {

    const history = useHistory();
    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="#home" onClick={() => {selectType('video'); history.push(`/`)}}>Reactube</Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link href="#home" onClick={() => {selectType('video'); history.push(`/`)}}>Videos</Nav.Link>
                <Nav.Link href="#features" onClick={() => {selectType('channel'); history.push(`/`)}}>Channels</Nav.Link>
            </Nav>
            <SearchBar onResults={onResults} type={selectedType}/>
        </Navbar>);
}
export default MyNav;