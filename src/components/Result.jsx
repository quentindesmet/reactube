import React from 'react';
import Card from 'react-bootstrap/Card';
import {Col, Image, Row} from 'react-bootstrap';
import api from "../lib/api";
import {useHistory} from "react-router-dom";

const Result = ({videoId, thumbnail, title, description, channelTitle, publishTime, selectVideo, type}) => {
    const history = useHistory();
    const search = async () => {
        let part = 'snippet,statistics,contentDetails,player,recordingDetails,topicDetails';
        if (type === 'channel') {
            part = 'snippet,statistics,contentDetails';
        }
        const resp = await api.get('/' + type + 's', {
            params: {
                id: videoId,
                part: part,
            }
        });
        console.log('Received', resp.data.items);
        selectVideo(resp.data.items[0]);
        history.push(`/${type}s/${videoId}`);
    };

    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Row>
                <Col xs={3}>
                    <Image src={thumbnail.url}
                           fluid={true}
                           rounded
                           onClick={search}/>
                </Col>
                <Col>
                    <Card.Title onClick={search}>{title}</Card.Title>
                    <Card.Subtitle>{channelTitle}</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text className={"mb-1 text-muted"}>
                        {publishTime}
                    </Card.Text>
                </Col>
            </Row>
        </Card.Body>
    </Card>)
};

export default Result;
